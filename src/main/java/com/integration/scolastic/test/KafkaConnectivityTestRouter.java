package com.integration.scolastic.test;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;



public class KafkaConnectivityTestRouter extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		from("direct:publish-to-kafka")
        .to("kafka:is-int1-kafka1.api.scholastic.com:9092,is-int1-kafka2.api.scholastic.com:9092,is-int1-kafka3.api.scholastic.com:9092?topic=test&serializerClass=kafka.serializer.StringEncoder");


		from("kafka:is-int1-kafka1.api.scholastic.com:9092?topic=test&zookeeperHost=10.45.151.242,10.45.155.161,10.45.152.158&groupId=x")
        .to("mock:result");
		
		/*from("file:///D:/input1/")
		.process(new Processor(){
            public void process(Exchange exchange) throws Exception {
                exchange.getIn().setBody(exchange.getIn().getBody(),String.class);
                System.out.println("file body "+exchange.getIn().getBody());
                exchange.getIn().setHeader(KafkaConstants.PARTITION_KEY, 0);
                exchange.getIn().setHeader(KafkaConstants.KEY, "1");
                
            }
        }).to("kafka:is-int1-kafka1.api.scholastic.com:9092?topic=test&groupId=testing&serializerClass=kafka.serializer.StringEncoder")
		.log("sucessfully sent message to kafka topic");
		
		from("kafka:is-int1-kafka1.api.scholastic.com:9092?topic=test&zookeeperHost=10.45.155.161&zookeeperPort=2181&groupId=testing&autoOffsetReset=smallest&consumersCount=1")
        .process(new Processor() {
            @Override
            public void process(Exchange exchange)
                    throws Exception {
                String messageKey = "";
                System.out.println("exchange*************** "+exchange.getIn().getBody());
                if (exchange.getIn() != null) {
                    Message message = exchange.getIn();
                    Integer partitionId = (Integer) message
                            .getHeader(KafkaConstants.PARTITION);
                    String topicName = (String) message
                            .getHeader(KafkaConstants.TOPIC);
                    if (message.getHeader(KafkaConstants.KEY) != null)
                        messageKey = (String) message
                                .getHeader(KafkaConstants.KEY);
                    String data = message.getBody(String.class);


                    System.out.println("topicName :: "
                            + topicName + " partitionId :: "
                            + partitionId + " messageKey :: "
                            + messageKey + " message :: "
                            + data + "\n");
                }
            }
        }).to("file:///D:/outputkafka");*/
		
		
		
	}

	
	
}
